import sublime_plugin
import json
import os

toggle = True

global __ColumbusLibrary


class Command:
    def __init__(self, command, add, example, description, parameters, syntax):
        self.command = command
        self.add = add
        self.example = example
        self.description = description
        self.parameters = parameters
        self.syntax = syntax

    def show(self):
        css = """html {background-color: #f0efe7; }
        body {font-size: 12px; }
        div.command {color: #2B000D; }
        div.add {color: #2C2F00; }
        div.example {color: #636B00; }
        div.description {color: #18094B; }
        div.parameters {color: #6635B2; }
        div.syntax {color: #FF7400; }
        h1 {font-size: 14px; }"""

        html = '<html><body><style>%s</style>' % css
        html += '''<div class="command"><h1>{1}</h1></div>
        <div class="example"><p>Example</p>{2}</div>
        <div class="description"><p>Description</p>{3}</div>
        <div class="parameters"><p>Parameters</p>{4}</div>
        <div class="syntax"><p>Syntax</p>{5}</div>'''.format(
            self.command, self.add, self.example,
            self.description, self.parameters, self.syntax)
        html += '</body></html>'
        return html


class ShowPopupCommand(sublime_plugin.EventListener):

    def __init__(self):
        global __ColumbusLibrary
        # Load the dictionary
        __ColumbusLibrary = dict()
        # Load the Command List
        str = os.path.join(os.path.dirname(__file__), 'CommandLineHelp.json')
        json_file = open(str)
        json_str = json_file.read()
        # print(json_str)
        # save it to the global dictionary
        json_commands = json.loads(json_str)
        # Adding commands to the library,
        # all Keys will be in lower() to simplify the search.
        for key, value in json_commands.items():
            __ColumbusLibrary[key.lower()] = Command(
                key, value['add'], value['example'],
                value['description'], value['parameters'], value['syntax']
                )

    def onLoad(self, view):
        global toggle
        toggle = True

    def on_selection_modified_async(self, view):

        global __ColumbusLibrary

        sel = view.sel()[0]
        # check if we have a selection
        if sel.empty():
            return

        for region in view.sel():
            str = view.substr(view.word(region))
            if str.lower() in __ColumbusLibrary:
                if toggle:
                    # Get the command and show it
                    command = __ColumbusLibrary[str.lower()]

                    view.show_popup(
                        command.show(), max_width=700, max_height=500
                        )
            else:
                view.hide_popup()


class toggleCommand(sublime_plugin.TextCommand):

    def run(self, edit):
        global toggle
        toggle = not toggle
