param([String]$file)

function Convert-FromRtf ($text)
{
    $Rtb = New-Object -TypeName System.Windows.Forms.RichTextBox
    $Rtb.Rtf = $text
    $Rtb.Text
}

$stream = (Get-Content $file) -join "`n" 
$stream = $stream.Replace("[NL][NL]","<br>")
$stream = $stream.Replace("[NL]","<br>")
$aux = ConvertFrom-Json $stream

foreach ($item in $aux.psobject.properties.Value) {
    $item.parameters  = Convert-FromRtf $item.parameters
}
# you need to open it in notepad++ and save it as "UTF-8 without BOM"
Convertto-json $aux | Out-File $file -Encoding UTF8