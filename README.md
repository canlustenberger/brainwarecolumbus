# SublimeText 3 Columbus (Brainware) Syntax#

>Columbus Software deployment is an Automated installation, update and removal of any software packages, drivers and configuration settings. Support for all current package formats, scripting and snapshots developed by [Brainware Group][0].

Script files are in \*.wms format witch is a mix between INI and BATCH.


## Features:
- Fully color \*.wms files
- Fully Syntax recognition
- Snippets for all the Commands

## Installation:

### Using [Package Control][1]

1. Click the `Tools > Command Palette` menu
2. Start `Package Control: Install Package`
3. Search for `Columbus Syntax`

### Manual Install

1. Click the `Preferences > Browse Packages...¦` menu
2. Browse up a folder and then into the `Installed Packages/` folder
3. Download [zip package][Download] rename it to `Columbus.sublime-package` and copy it into the `Installed Packages/` directory

## Support:

- Any bugs please feel free to report [here][issues].

[0]: https://www.brainwaregroup.com/en/
[1]: https://packagecontrol.io/
[Download]: https://bitbucket.org/canlustenberger/columbus/get/master.zip
[issues]: https://bitbucket.org/canlustenberger/columbus/issues?status=new&status=open